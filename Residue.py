# Copyright (C) 2002, Thomas Hamelryck (thamelry@binf.ku.dk)
# This code is part of the Biopython distribution and governed by its
# license.  Please see the LICENSE file that should have been included
# as part of this package.

"""Residue class, used by Structure objects."""

# My Stuff
from Bio.PDB.PDBExceptions import PDBConstructionException
from Bio.PDB.Entity import Entity, DisorderedEntityWrapper
from Bio.PDB.Vector import calc_dihedral, calc_angle
import math
import numpy as np

_atom_name_dict = {}
_atom_name_dict["N"] = 1
_atom_name_dict["CA"] = 2
_atom_name_dict["C"] = 3
_atom_name_dict["O"] = 4

chi_atoms = dict(
    chi1=dict(
        ARG=['N', 'CA', 'CB', 'CG'],
        ASN=['N', 'CA', 'CB', 'CG'],
        ASP=['N', 'CA', 'CB', 'CG'],
        CYS=['N', 'CA', 'CB', 'SG'],
        GLN=['N', 'CA', 'CB', 'CG'],
        GLU=['N', 'CA', 'CB', 'CG'],
        HIS=['N', 'CA', 'CB', 'CG'],
        ILE=['N', 'CA', 'CB', 'CG1'],
        LEU=['N', 'CA', 'CB', 'CG'],
        LYS=['N', 'CA', 'CB', 'CG'],
        MET=['N', 'CA', 'CB', 'CG'],
        PHE=['N', 'CA', 'CB', 'CG'],
        PRO=['N', 'CA', 'CB', 'CG'],
        SER=['N', 'CA', 'CB', 'OG'],
        THR=['N', 'CA', 'CB', 'OG1'],
        TRP=['N', 'CA', 'CB', 'CG'],
        TYR=['N', 'CA', 'CB', 'CG'],
        VAL=['N', 'CA', 'CB', 'CG1'],
    ),
    #altchi1=dict(
    #    VAL=['N', 'CA', 'CB', 'CG2'],
    #   ),
    chi2=dict(
        ARG=['CA', 'CB', 'CG', 'CD'],
        ASN=['CA', 'CB', 'CG', 'OD1'],
        ASP=['CA', 'CB', 'CG', 'OD1'],
        GLN=['CA', 'CB', 'CG', 'CD'],
        GLU=['CA', 'CB', 'CG', 'CD'],
        HIS=['CA', 'CB', 'CG', 'ND1'],
        ILE=['CA', 'CB', 'CG1', 'CD1'],
        LEU=['CA', 'CB', 'CG', 'CD1'],
        LYS=['CA', 'CB', 'CG', 'CD'],
        MET=['CA', 'CB', 'CG', 'SD'],
        PHE=['CA', 'CB', 'CG', 'CD1'],
        PRO=['CA', 'CB', 'CG', 'CD'],
        TRP=['CA', 'CB', 'CG', 'CD1'],
        TYR=['CA', 'CB', 'CG', 'CD1'],
    ),
    #altchi2=dict(
    #    ASP=['CA', 'CB', 'CG', 'OD2'],
    #    LEU=['CA', 'CB', 'CG', 'CD2'],
    #    PHE=['CA', 'CB', 'CG', 'CD2'],
    #    TYR=['CA', 'CB', 'CG', 'CD2'],
    #),
    chi3=dict(
        ARG=['CB', 'CG', 'CD', 'NE'],
        GLN=['CB', 'CG', 'CD', 'OE1'],
        GLU=['CB', 'CG', 'CD', 'OE1'],
        LYS=['CB', 'CG', 'CD', 'CE'],
        MET=['CB', 'CG', 'SD', 'CE'],
    ),
    chi4=dict(
        ARG=['CG', 'CD', 'NE', 'CZ'],
        LYS=['CG', 'CD', 'CE', 'NZ'],
    ),
    chi5=dict(
        ARG=['CD', 'NE', 'CZ', 'NH1'],
    ),
)

class Residue(Entity):
    """
    Represents a residue. A Residue object stores atoms.
    """
    def __init__(self, id, resname, segid):
        self.level = "R"
        self.disordered = 0
        self.resname = resname
        self.segid = segid
        Entity.__init__(self, id)

    # Special methods

    def __repr__(self):
        resname = self.get_resname()
        hetflag, resseq, icode = self.get_id()
        full_id = (resname, hetflag, resseq, icode)
        return "<Residue %s het=%s resseq=%s icode=%s>" % full_id

    # Private methods

    def _sort(self, a1, a2):
        """Sort the Atom objects.

        Atoms are sorted alphabetically according to their name,
        but N, CA, C, O always come first.

        Arguments:
        o a1, a2 - Atom objects
        """
        name1 = a1.get_name()
        name2 = a2.get_name()
        if name1 == name2:
            return(cmp(a1.get_altloc(), a2.get_altloc()))
        if name1 in _atom_name_dict:
            index1 = _atom_name_dict[name1]
        else:
            index1 = None
        if name2 in _atom_name_dict:
            index2 = _atom_name_dict[name2]
        else:
            index2 = None
        if index1 and index2:
            return cmp(index1, index2)
        if index1:
            return -1
        if index2:
            return 1
        return cmp(name1, name2)

    ## returns a list of atoms in rotamers for current residue
    def _rotamers(self):
        rotamers = {}
        for chi_level in chi_atoms.keys():
            if self.resname in chi_atoms[chi_level].keys():
                rotamer_tmp = []
                expected_rotamer_atms = chi_atoms.get(chi_level, {}).get(self.resname) 
                for atm1 in expected_rotamer_atms:
                    for atm2 in self:
                        if atm1 == atm2.get_name():
                            rotamer_tmp.append(atm2)
                        continue
                if len(rotamer_tmp) == 4:
                    rotamers[chi_level] = rotamer_tmp
                else:
                    continue
            else:
                continue
        return rotamers


    # Public methods

    def add(self, atom):
        """Add an Atom object.

        Checks for adding duplicate atoms, and raises a
        PDBConstructionException if so.
        """
        atom_id = atom.get_id()
        if self.has_id(atom_id):
            raise PDBConstructionException(
                "Atom %s defined twice in residue %s" % (atom_id, self))
        Entity.add(self, atom)

    def sort(self):
        self.child_list.sort(self._sort)

    def flag_disordered(self):
        "Set the disordered flag."
        self.disordered = 1

    def is_disordered(self):
        "Return 1 if the residue contains disordered atoms."
        return self.disordered

    def get_resname(self):
        return self.resname

    def get_unpacked_list(self):
        """
        Returns the list of all atoms, unpack DisorderedAtoms."
        """
        atom_list = self.get_list()
        undisordered_atom_list = []
        for atom in atom_list:
            if atom.is_disordered():
                undisordered_atom_list = (undisordered_atom_list + atom.disordered_get_list())
            else:
                undisordered_atom_list.append(atom)
        return undisordered_atom_list
    def get_segid(self):
        return self.segid

    def get_pdb_seq_num(self):
        resseq = self.get_id()[1]
	return resseq

    def get_atom(self):
        for a in self:
            yield a

    ## returns a list of five elements, chi angles or NA, one for possible rotamer..
    ## if the residue is disordered it returns five "Disordered".
    ## DOES NOT CALCULATE ALTCHI1 and 2.
    def get_chi(self):
        self.sort()
	rotamers = self._rotamers()	
        if self.is_disordered() == 1 or len(rotamers.keys()) == 0:
            return ['Disordered'] * 5
        chi_angles = {}
        for rotamer in rotamers.keys():
            vector1 = rotamers[rotamer][0].get_vector()
            vector2 = rotamers[rotamer][1].get_vector()
            vector3 = rotamers[rotamer][2].get_vector()
            vector4 = rotamers[rotamer][3].get_vector()
            chi_angle = round(math.degrees(calc_dihedral(vector1, vector2, vector3, vector4)),1)
            chi_angles[rotamer] = chi_angle
        alt_lst = ('altchi1', 'altchi2')
        chi_lst = ('chi1', 'chi2')
     ## returns only the smaller angle if both chi and altchi are present
        #for alt, chi in zip(alt_lst, chi_lst):
	#    if alt in chi_angles and chi in chi_angles:
        #        min_angle = min(abs(chi_angles.get(alt)), abs(chi_angles.get(chi)))
        #        chi_angles[chi] = min_angle
        #        del chi_angles[alt] 
        chi = [value for(key, value) in sorted(chi_angles.items())]
        if len(chi) != 5:
            return chi + ['NA'] * (5 - len(chi))
        else:
            return chi

    # returns a list of 5 elements, mean isotropic B factor for atoms composing each possible rotamer.
    def get_mean_bfactor(self):
        self.sort()
        rotamers = self._rotamers()
        if self.is_disordered() == 1 or len(rotamers.keys()) == 0:
            return ['Disordered'] * 5
        chi_bfactors = {}
        for rotamer in rotamers.keys():
            chi_bfactors[rotamer] = np.mean([a.get_bfactor() for a in rotamers[rotamer]])
        bfactor = [value for(key, value) in sorted(chi_bfactors.items())]	
        if len(bfactor) != 5:
            return bfactor + ['NA'] * (5 - len(bfactor))
        else:
            return bfactor


class DisorderedResidue(DisorderedEntityWrapper):
    """
    DisorderedResidue is a wrapper around two or more Residue objects. It is
    used to represent point mutations (e.g. there is a Ser 60 and a Cys 60 residue,
    each with 50 % occupancy).
    """
    def __init__(self, id):
        DisorderedEntityWrapper.__init__(self, id)

    def __repr__(self):
        resname = self.get_resname()
        hetflag, resseq, icode = self.get_id()
        full_id = (resname, hetflag, resseq, icode)
        return "<DisorderedResidue %s het=%s resseq=%i icode=%s>" % full_id

    def add(self, atom):
        residue = self.disordered_get()
        if not atom.is_disordered() == 2:
            # Atoms in disordered residues should have non-blank
            # altlocs, and are thus represented by DisorderedAtom objects.
            resname = residue.get_resname()
            het, resseq, icode = residue.get_id()
            # add atom anyway, if PDBParser ignores exception the atom will be part of the residue
            residue.add(atom)
            raise PDBConstructionException(
                "Blank altlocs in duplicate residue %s (%s, %i, %s)"
                % (resname, het, resseq, icode))
        residue.add(atom)

    def sort(self):
        "Sort the atoms in the child Residue objects."
        for residue in self.disordered_get_list():
            residue.sort()

    def disordered_add(self, residue):
        """Add a residue object and use its resname as key.

        Arguments:
        o residue - Residue object
        """
        resname = residue.get_resname()
        # add chain parent to residue
        chain = self.get_parent()
        residue.set_parent(chain)
        assert(not self.disordered_has_id(resname))
        self[resname] = residue
        self.disordered_select(resname)
