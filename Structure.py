# Copyright (C) 2002, Thomas Hamelryck (thamelry@binf.ku.dk)
# This code is part of the Biopython distribution and governed by its
# license.  Please see the LICENSE file that should have been included
# as part of this package.

"""The structure class, representing a macromolecular structure."""

from Bio.PDB.Entity import Entity
from Bio.Seq import Seq
from Bio.Alphabet import generic_protein


class Structure(Entity):
    """
    The Structure class contains a collection of Model instances.
    """
    def __init__(self, id, can_sequence):
        self.level = "S"
        ml = max(len(s) for s in can_sequence)
        ## take the longest sequence among canonical sequences
        ##longest_seq = list(set(s for s in can_sequence if len(s) == ml)) 
        ##self.can_sequence = str(longest_seq[0])
	## if can_sequence is a list of single-letter AA print it as a string, 
	## if there are multiple sequences print them separated by ',' 
	if ml == 1:
		self.can_sequence = ''.join(map(str, can_sequence))
	else:
        	self.can_sequence = ','.join(map(str, can_sequence))
        Entity.__init__(self, id)
          
    # Special methods

    def __repr__(self):
        return "<Structure id=%s>" % self.get_id()

    # Private methods

    def _sort(self, m1, m2):
        """Sort models.

        This sorting function sorts the Model instances in the Structure instance.
        The sorting is done based on the model id, which is a simple int that
        reflects the order of the models in the PDB file.

        Arguments:
        o m1, m2 - Model instances
        """
        return cmp(m1.get_id(), m2.get_id())

    # Public
    ## returns a Seq object containing the canonical sequence
    def get_can_sequence(self):
        seq = Seq(self.can_sequence, generic_protein)
        return seq

    def get_models(self):
        for m in self:
            yield m

    def get_chains(self):
        for m in self.get_models():
            for c in m:
                yield c

    def get_residues(self):
        for c in self.get_chains():
            for r in c:
                yield r

    def get_atoms(self):
        for r in self.get_residues():
            for a in r:
                yield a
